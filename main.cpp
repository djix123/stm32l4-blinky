#include <errno.h>

#ifndef STM32L4
#define STM32L4
#endif

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/flash.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/cm3/systick.h>

extern "C" {
    void sys_tick_handler(void);
}

static void clock_setup() {
        rcc_osc_on(RCC_HSI16);
        rcc_wait_for_osc_ready(RCC_HSI16);

        rcc_set_sysclk_source(RCC_CFGR_SW_HSI16);
        rcc_wait_for_sysclk_status(RCC_HSI16);
     
        flash_prefetch_enable();
        flash_set_ws(4);
        flash_dcache_enable();
        flash_icache_enable();
        
        /* 16MHz / 4 = > 4 * 40 = 160MHz VCO => 80MHz main pll  */
        rcc_set_main_pll(
            RCC_PLLCFGR_PLLSRC_HSI16, // Select internal 16Mhz clock
            4,                          // Set m = 4 (/m)
            40,                         // Set n = 40 (*n)
            RCC_PLLCFGR_PLLP_DIV7,      // Set p = 7 (/p) PLLSAI3CLK = ((16/m)*n)/p 
            RCC_PLLCFGR_PLLQ_DIV2,      // Set q = 2 (/q) PLL48M1CLK = ((16/m)*n)/q
            RCC_PLLCFGR_PLLR_DIV2);     // Set r = 2 (/r) PLLCLK = ((16/m)*n)/r
        rcc_osc_on(RCC_PLL);
        rcc_wait_for_osc_ready(RCC_PLL);

        rcc_set_sysclk_source(RCC_CFGR_SW_PLL);
        rcc_wait_for_sysclk_status(RCC_PLL);
       
        rcc_set_hpre(RCC_CFGR_HPRE_DIV16);      // AHB clock = sysclock / 16
        rcc_set_ppre1(RCC_CFGR_PPRE1_NODIV);    // APB1 clock = AHB clock
        rcc_set_ppre2(RCC_CFGR_PPRE2_NODIV);    // APB2 clock = AHB clock
        
        rcc_ahb_frequency = (((16000000 / 4) * 40) / 2) / 16;
        rcc_apb1_frequency = rcc_ahb_frequency;
        rcc_apb2_frequency = rcc_ahb_frequency;

        rcc_periph_clock_enable(RCC_GPIOB);
}

static void systick_setup() {
    // Set the systick clock source to our main clock
    systick_set_clocksource(STK_CSR_CLKSOURCE_AHB_DIV8);
    // Clear the Current Value Register so that we start at 0
    STK_CVR = 0;
    // In order to trigger an interrupt every millisecond, we can set the reload
    // value to be the speed of the processor / 1000 -1
    systick_set_reload( (rcc_ahb_frequency / 8) / 1000 - 1);
    // Enable interrupts from the system tick clock
    systick_interrupt_enable();
    // Enable the system tick counter
    systick_counter_enable();
}

// Storage for our monotonic system clock.
// Note that it needs to be volatile since we're modifying it from an interrupt.
static volatile uint64_t _millis = 0;

uint64_t millis() {
    return _millis;
}

// This is our interrupt handler for the systick reload interrupt.
// The full list of interrupt services routines that can be implemented is
// listed in libopencm3/include/libopencm3/stm32/f0/nvic.h
void sys_tick_handler(void) {
    // Increment our monotonic clock
    _millis++;
}

/**
 * Delay for a real number of milliseconds
 */
void delay(uint64_t duration) {
    const uint64_t until = millis() + duration;
    while (millis() < until);
}

static void gpio_setup() {
    // Our test LED is connected to Port A pin 11, so let's set it as output
    gpio_mode_setup(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO3);
}

int main() {
    clock_setup();
    systick_setup();
    gpio_setup();

    // Toggle the LED on and off forever
    while (1) {
        gpio_set(GPIOB, GPIO3);
        delay(500);
        gpio_clear(GPIOB, GPIO3);
        delay(500);
    }

    return 0;
}
